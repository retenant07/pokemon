## Pokemon Application

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. <br />

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
 
### Deployment

[https://ritpokemon.netlify.com/](https://ritpokemon.netlify.com/)

### `Features`
1. Display pokemon list in the home page. <br />
2. Search Pokemon with thier name (for now). <br />
3. Show pokemon details like its profile, abilities and moves

### `TODO`
1. Search pokemon by habitat, region
2. Pagination implementation for listing all ~1000 pokemons

