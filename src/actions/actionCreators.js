import axios from 'axios';
import { FETCH_START, FETCH_POKEMONS, CLEAR_POKEMONS, FETCH_POKEMON_DETAIL, FETCH_POKEMON_SPECIES, SEARCH_FILTER, POKEMON_ERROR } from './actionTypes';

export const clearPokemonList= () => ({
  type: CLEAR_POKEMONS
});

export const clearSearchText= () => ({
  type: 'CLEAR_TEXT'
});

export const handleSearch = (text) => ({
  type: SEARCH_FILTER,
  payload: { text }
});

export const fetchPokemonList = () => {
  return async dispatch => {
    dispatch({ type: FETCH_START });
    try {
      const response = await axios.get('https://pokeapi.co/api/v2/pokemon?limit=150');
      dispatch({ type: FETCH_POKEMONS, payload: response.data.results });
    } catch {
      dispatch({ type: POKEMON_ERROR });
    }
  };
};

export const fetchPokemonDetail = (id) => {
  return async dispatch => {
    dispatch({ type: FETCH_START });
    try {
      const res1 = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}/`);
      const res2 = await axios.get(`https://pokeapi.co/api/v2/pokemon-species/${id}/`);
      dispatch({ type: FETCH_POKEMON_DETAIL, payload: {profile: res1.data, species: res2.data} });
    } catch {
      dispatch({ type: POKEMON_ERROR });
    }
  }
}

export const fetchPokemonSpecies = (id) => {
  return async dispatch => {
    dispatch({ type: FETCH_START });
    try {
      const response = await axios.get(`https://pokeapi.co/api/v2/pokemon-species/${id}/`);
      dispatch({ type: FETCH_POKEMON_SPECIES, payload: response.data });
    } catch {
      dispatch({ type: POKEMON_ERROR });
    }
  }
}