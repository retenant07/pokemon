import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PokemonList from './containers/PokemonList';
import PokemonDetail from './containers/PokemonDetail';
import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path='/' component={PokemonList} />
        <Route exact path='/pokemon/:id' component={PokemonDetail} />
      </Switch>
    </div>
  );
}

export default App;
