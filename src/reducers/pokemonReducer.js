import { FETCH_START, FETCH_POKEMON_DETAIL, FETCH_POKEMON_SPECIES } from '../actions/actionTypes';

const initialData = {
  status: 'default',
  profile: {}, 
  species: {}
};

const pokemonReducer = (state = initialData, action) => {
  switch (action.type) {
    case FETCH_START:
      return { ...state, status: 'loading' };

    case FETCH_POKEMON_DETAIL:
      return { ...state, status: 'completed', profile: action.payload.profile, species: action.payload.species };

    case FETCH_POKEMON_SPECIES:
      return { ...state, status: 'species_success', species: action.payload };
      
    default:
      return state;
  }
}

export default pokemonReducer;