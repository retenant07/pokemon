import { FETCH_START, FETCH_POKEMONS, CLEAR_POKEMONS, POKEMON_ERROR } from '../actions/actionTypes';

const initialData = {
  status: 'default',
  pokemons: {}
};

const pokemonListReducer = (state = initialData, action) => {
  switch (action.type) {
    case FETCH_START:
      return { ...state, status: 'loading' };

    case FETCH_POKEMONS:
      return { ...state, status: 'completed', pokemons: action.payload };

    case POKEMON_ERROR:
      return { ...state, status: 'error' }

    case CLEAR_POKEMONS:
    default:
      return state;
  }
}

export default pokemonListReducer;