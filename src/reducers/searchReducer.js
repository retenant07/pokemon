import { SEARCH_FILTER, SHOW_ALL } from '../actions/actionTypes';
const initialFilter = {
  type: SHOW_ALL,
  text: ''
};
const searchReducer = (state = initialFilter, action) => {
  switch (action.type) {
    case SEARCH_FILTER:
      return {
        type: action.type,
        text: action.payload.text
      }
    case 'CLEAR_TEXT':
      return initialFilter;
    default:
      return state;
  }
}

export default searchReducer;