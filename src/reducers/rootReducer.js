import { combineReducers } from 'redux';
import pokemonList from './pokemonListReducer';
import pokemonDetail from './pokemonReducer';
import searchFilter from './searchReducer';

export default combineReducers({
  pokemonList,
  pokemonDetail,
  searchFilter
});