import React from 'react';
import { Row, Col, Progress, Divider } from 'antd';
import {
  getHeight, getWeight, getGenderRatioFemale, getGenderRatioMale, getGrowthRate,
  getCatchRate, getHatchSteps, getAbilities, getEggGroups, TYPE_COLORS
} from './Constant';


const PokemonListItem = (props) => {
  const pokemon = props.profile;
  const species = props.species;

  return (
    <>
        <Row>
        <Col span={4}><img alt={pokemon.name} src={require(`../sprites/${pokemon.id}.png`)} /></Col>
        <Col span={20}>
          <Row className="pokemonType">
             {pokemon.types.map((type, index) => (
            <Col span={6} key={index} style={{ backgroundColor: `#${TYPE_COLORS[type.type.name]}`, color: 'white' }}>
              {type.type.name.toLowerCase().split(' ').map(s => s.charAt(0).toUpperCase() + s.substring(1)).join(' ')}
            </Col>
          ))}
          </Row>
        </Col>
        </Row>
        <Row>
          <Col span={24}><Divider className="divider"><strong className="dividerTitle">{pokemon.name + ' stats'}</strong></Divider></Col>
        </Row>
        
        <Row>
        <Col span={12} offset={6}>
        <Row>
          <Progress percent={pokemon.stats[0].base_stat} width={40} />
          <p>{pokemon.stats[0].stat.name}</p>
          <Progress percent={pokemon.stats[1].base_stat} width={40} />
          <p>{pokemon.stats[1].stat.name}</p>
          <Progress percent={pokemon.stats[2].base_stat} width={40} />
          <p>{pokemon.stats[2].stat.name}</p>
          <Progress percent={pokemon.stats[3].base_stat} width={40} />
          <p>{pokemon.stats[3].stat.name}</p>
          <Progress percent={pokemon.stats[4].base_stat} width={40} />
          <p>{pokemon.stats[4].stat.name}</p>
          <Progress percent={pokemon.stats[5].base_stat} width={40} />
          <p>{pokemon.stats[5].stat.name}</p>
        </Row>
        </Col>
        </Row>
        <Row>
          <Col span={24}><Divider className="divider"><strong className="dividerTitle">Profile</strong></Divider></Col>
        </Row>
        <Row>
        <Col span={12} offset={6}>
          <Row><strong>Height :</strong><p>{getHeight(pokemon.height) + 'ft'}</p></Row>
          <Row><strong>Weight :</strong><p>{getWeight(pokemon.weight) + 'lbs'}</p></Row>
          <Row><strong>Hatch Steps :</strong><p>{getHatchSteps(species.hatch_counter)}</p></Row>
          <Row><strong>Catch Rate :</strong><p>{getCatchRate(species.capture_rate)}</p></Row>
          <Row><strong>Eggs Groups :</strong><p>{getEggGroups(species.egg_groups)}</p></Row>
          <Row><strong>Abilities : </strong><p>{getAbilities(pokemon.abilities)}</p></Row>
          <Row><strong>Growth Rate :</strong><p>{getGrowthRate(species.growth_rate.name)}</p></Row>
          <Row><strong>Gender Ratio(Male/Female) :</strong><Progress type="line" percent={getGenderRatioMale(species.gender_rate)}/><Progress type="line" percent={getGenderRatioFemale(species.gender_rate)}></Progress></Row>
        </Col>
      </Row>
    </>
  );
}
export default PokemonListItem;