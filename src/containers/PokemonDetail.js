import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchPokemonDetail } from '../actions/actionCreators';
import { Layout, Menu, Spin } from 'antd';
import PokemonProfile from './PokemonProfile';
import PokemonMoves from './PokemonMoves';

export const PokemonDetail = (props) => {
  const pokemonId = props.match.params.id;
  const { Content, Sider } = Layout;
  const { SubMenu } = Menu;

  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(fetchPokemonDetail(pokemonId));
  }, [pokemonId, dispatch]);

  const [currentPage, setCurrentPage] = React.useState('profile');

  const { profile, species, status } = useSelector(state => {
    return state.pokemonDetail;
  })

  const handleClick = e => {
    setCurrentPage(e.key);
  };

  return (
    <>
    {status === 'completed' ?
      <Layout>
        <Content>
          <Layout className="site-layout-background" style={{ padding: '24px 0' }}>
            <Sider className="site-layout-background" width={150}>
              <Menu
                onClick={handleClick}
                mode="inline"
                defaultSelectedKeys={['profile']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%' }}>
                <SubMenu
                  key="sub1"
                  title={<span className="menuPokemonTitle"><strong>{profile.name}</strong></span>}>
                  <Menu.Item key="profile">Profile</Menu.Item>
                  <Menu.Item key="moves">Moves</Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Content className="pokemonDetailContent" span={18}>
              {currentPage === 'profile' && <PokemonProfile profile={profile} species={species} />}
              {currentPage === 'moves' && <PokemonMoves profile={profile} />}
            </Content>
          </Layout>
        </Content>
      </Layout> : <Spin size="large" />}
    </>
  )
}

export default PokemonDetail;