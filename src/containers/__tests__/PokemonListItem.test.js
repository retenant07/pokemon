import React from "react";
import { mount } from "enzyme";
import PokemonListItem from "../PokemonListItem";
import { BrowserRouter } from 'react-router-dom';

const data = {
  "name": "bulbasaur",
  "url": "https://pokeapi.co/api/v2/pokemon/1/"
}

describe("Component renders properly", () => {
  it("Test MovieListItem Value", () => {
    const wrapper = mount(<BrowserRouter><PokemonListItem id={1} pokemon={data} /></BrowserRouter>);
    expect(wrapper.html()).toMatchSnapshot();
    expect(wrapper.find("img").prop("src")).toEqual('1.png');
    expect(wrapper.find('.ant-card-meta-title').text()).toEqual('bulbasaur');
  });
});