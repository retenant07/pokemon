import React from "react";
import { shallow } from "enzyme";
import SearchFilter from "../SearchFilter";

describe("Component renders properly", () => {
  it("Test SearchFilter Component", () => {
    const handleSearchText = jest.fn();
    const wrapper = shallow(<SearchFilter handleSearchText={handleSearchText} />);
    expect(wrapper.html()).toMatchSnapshot();
    wrapper.find('.searchInput').at(0).simulate('change', { target: {value: 'MR' }});
    expect(handleSearchText).toHaveBeenCalled();
  });
});