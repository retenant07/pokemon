import React from "react";
import { mount, shallow } from "enzyme";
import MovesItem from "../MovesItem";

const moves = [{
  "move": {
    "name": "razor-wind",
    "url": "https://pokeapi.co/api/v2/move/13/"
  },
  "version_group_details": [
    {
      "level_learned_at": 0,
      "move_learn_method": {
        "name": "egg",
        "url": "https://pokeapi.co/api/v2/move-learn-method/2/"
      },
      "version_group": {
        "name": "crystal",
        "url": "https://pokeapi.co/api/v2/version-group/4/"
      }
    },
    {
      "level_learned_at": 0,
      "move_learn_method": {
        "name": "egg",
        "url": "https://pokeapi.co/api/v2/move-learn-method/2/"
      },
      "version_group": {
        "name": "gold-silver",
        "url": "https://pokeapi.co/api/v2/version-group/3/"
      }
    }
  ]
}, 
{
  "move": {
    "name": "razor-wind2",
    "url": "https://pokeapi.co/api/v2/move/14/"
  },
  "version_group_details": [
    {
      "level_learned_at": 0,
      "move_learn_method": {
        "name": "egg",
        "url": "https://pokeapi.co/api/v2/move-learn-method/2/"
      },
      "version_group": {
        "name": "crystal",
        "url": "https://pokeapi.co/api/v2/version-group/4/"
      }
    },
    {
      "level_learned_at": 0,
      "move_learn_method": {
        "name": "egg",
        "url": "https://pokeapi.co/api/v2/move-learn-method/2/"
      },
      "version_group": {
        "name": "gold-silver",
        "url": "https://pokeapi.co/api/v2/version-group/3/"
      }
    }
  ]
}]

describe("Component renders properly", () => {
  it("Test MovesItem Value", () => {
    const wrapper = shallow(<MovesItem id={1} moves={moves} />);
    expect(wrapper.html()).toMatchSnapshot();
  });
});