import React from "react";
import { shallow, mount } from "enzyme";
import configureStore from "redux-mock-store";
import thunk from 'redux-thunk';
import * as ReactReduxHooks from "../react-redux-hooks";
import PokemonList from "../PokemonList";
import data from '../__mockData__/pokemonStoreData.json';
import pokemons from '../__mockData__/pokemon.json';
import PokemonListItem from "../PokemonListItem";
import { Provider } from 'react-redux';
import axios from 'axios';
import { BrowserRouter } from 'react-router-dom';

window.matchMedia = jest.fn().mockImplementation((query) => ({
  matches: false,
  media: query,
  onchange: null,
  addListener: jest.fn(), // deprecated
  removeListener: jest.fn(), // deprecated
  addEventListener: jest.fn(),
  removeEventListener: jest.fn(),
  dispatchEvent: jest.fn(),
}))

jest.mock('axios');
describe("Movie List", () => {
  let wrapper;
  let useEffect;
  let store;

  const mockUseEffect = () => {
    useEffect.mockImplementationOnce(f => f());
  };
  beforeEach(() => {
    store = configureStore([thunk])(data);
    useEffect = jest.spyOn(React, "useEffect");
    mockUseEffect(); // 2 times
    mockUseEffect(); //   

    jest
      .spyOn(ReactReduxHooks, "useSelector")
      .mockImplementation(state => store.getState());

    jest
      .spyOn(ReactReduxHooks, "useDispatch")
      .mockImplementation(() => store.dispatch);

    axios.get.mockImplementationOnce(() => Promise.resolve(pokemons));
    wrapper = mount(<Provider store={store}><BrowserRouter><PokemonList /></BrowserRouter></Provider>);
  });

  it("pokemon list render properly", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should render PokemonListItem components with movies", () => {
    expect(wrapper.find(PokemonListItem)).toHaveLength(3);
  });
}); 