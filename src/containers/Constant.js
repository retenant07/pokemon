import { SEARCH_FILTER, SHOW_ALL } from '../actions/actionTypes';
export const TYPE_COLORS = {
  bug: 'B1C12E',
  dark: '4F3A2D',
  dragon: '755EDF',
  electric: 'FCBC17',
  fairy: 'F4B1F4',
  fighting: '823551D',
  fire: 'E73B0C',
  flying: 'A3B3F7',
  ghost: '6060B2',
  grass: '74C236',
  ground: 'D3B357',
  ice: 'A3E7FD',
  normal: 'C8C4BC',
  poison: '934594',
  psychic: 'ED4882',
  rock: 'B9A156',
  steel: 'B5B5C3',
  water: '3295F6'
};

export const getHeight = (height) => Math.round((height * 0.328084 + 0.00001) * 100) / 100;

export const getWeight = (weight) => Math.round((weight * 0.220462 + 0.00001) * 100) / 100;

export const getGenderRatioFemale = (rate) => 12.5 * rate;

export const getGenderRatioMale = (rate) => 12.5 * (8 - rate);

export const getCatchRate = (rate) => Math.round((100 / 255) * rate);

export const getHatchSteps = (counter) => 255 * (counter + 1);

export const getAbilities = (abilities) => abilities
  .map(ability => {
    return ability.ability.name
      .toLowerCase()
      .split('-')
      .map(s => s.charAt(0).toUpperCase() + s.substring(1))
      .join(' ');
  }).join(', ');

export const getEggGroups = (eggGroups) => eggGroups
  .map(group => {
    return group.name
      .toLowerCase()
      .split(' ')
      .map(s => s.charAt(0).toUpperCase() + s.substring(1))
      .join(' ');
  }).join(', ');

export const getFilteredList = (pokemons, searchType, searchText) => {
  switch (searchType) {
    case SHOW_ALL:
      return pokemons;
    case SEARCH_FILTER:
      return pokemons.flat().filter(item => {
        let title = item.name.toLowerCase();
        return title.indexOf(searchText.toLowerCase()) > -1;
      });
    default:
      return pokemons;
  }
}

export const getGrowthRate = (rate) => rate.toLowerCase().split(' ').map(s => s.charAt(0).toUpperCase() + s.substring(1)).join('');