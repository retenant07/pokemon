import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Row, Spin } from 'antd';
import { fetchPokemonList, clearPokemonList, handleSearch, clearSearchText } from '../actions/actionCreators';
import SearchFilter from './SearchFilter';
import PokemonListItem from './PokemonListItem';
import { FileSearchOutlined } from '@ant-design/icons';
import { getFilteredList } from './Constant';

const PokemonList = () => {
  const dispatch = useDispatch();
  const { pokemons, status } = useSelector(state => state.pokemonList);
  const { type, text } = useSelector(state => state.searchFilter);
  const pokemonList = getFilteredList(pokemons, type, text);

  React.useEffect(() => {
    dispatch(fetchPokemonList());
    return () => {
      dispatch(clearPokemonList());
      dispatch(clearSearchText());
    }
  }, [dispatch]);

  const handleSearchText = (text) => {
    dispatch(handleSearch(text));
    return () => {
      dispatch(clearSearchText());
    }
  }

  return (
    <div className="pokemon-container">
      <Row className="pokemonSearch">
        <SearchFilter handleSearchText={handleSearchText} />
      </Row>
      {status === 'completed' ?
        <>
          <h1 className="pokemonTitle"><strong>Pokemons</strong></h1>
          <Row className="pokemonList">
            {pokemonList && pokemonList.length > 0 ? pokemonList.map((item, index) => (
              <PokemonListItem key={item.name} id={index + 1} pokemon={item} />
            )) : <div className="noPokemonFound"><FileSearchOutlined /><p>No POKEMON Found</p></div>}
          </Row></>
        : <Spin size="large" />
      }
    </div>
  )
}

export default PokemonList;