import React from 'react';
import { Input } from 'antd';

const SearchFilter = (props) => {
  const handleInputChange = ((e) => {
    props.handleSearchText(e.target.value);
  });

  return (
    <>
      <div className="search-form">
        <img src={require(`../sprites/pokemon.png`)} alt='pokemon'/>
        <Input className="searchInput" type="text" placeholder="Search your pokemon with their names..."
          onChange={handleInputChange} />
      </div>
    </>
  );
}

export default SearchFilter;