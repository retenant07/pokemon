import React from 'react';
import {Tabs } from 'antd';
import MovesItem from './MovesItem';

const PokemonMoves = (props) => {
  const { TabPane } = Tabs;
  return (
    <>
      <Tabs defaultActiveKey="natural">
        <TabPane tab="Natural" key="natural">
          <MovesItem moves={getFilteredList(props.profile.moves, 'level-up')} />
        </TabPane>
        <TabPane tab="Machine" key="machine">
          <MovesItem moves={getFilteredList(props.profile.moves, 'machine')} />
        </TabPane>
        <TabPane tab="Tutor" key="tutor">
          <MovesItem moves={getFilteredList(props.profile.moves, 'tutor')} />
        </TabPane>
        <TabPane tab="Eggs" key="eggs">
          <MovesItem moves={getFilteredList(props.profile.moves, 'egg')} />
        </TabPane>
      </Tabs>
    </>
  );
}

const getFilteredList = (moves, move) => {
  //return moves.filter(item => item['version_group_details'].find(detail => detail['move_learn_method'].name === move))
  return moves.filter(item => item.version_group_details[0].move_learn_method.name === move);
}
export default PokemonMoves;