import React from 'react';
import { Col, Card } from 'antd';
import { Link } from 'react-router-dom';

const PokemonListItem = ({ id, pokemon }) => {
  const { Meta } = Card;

  return (
    <>
      <Col key={pokemon.name} className="gutter-row" span={4}>
        <Link to={{ pathname: `/pokemon/${id}`}}>
        <Card hoverable
          style={{ width: 200 }}
          cover={<img src={require(`../sprites/${id}.png`)} className="card-img-top" alt={pokemon.name} style={{ width: '5em', height: '7em' }}/>}>
          <Meta title={pokemon.name} />
        </Card>
        </Link>
      </Col>
    </>
  );
}
export default PokemonListItem;