import React from 'react';
import { List, Card } from 'antd';

const MovesItem = ({ moves }) => {
  return (
    <List className="moveGrid"
      grid={{ gutter: 16, column: 4 }}
      dataSource={moves}
      renderItem={(item, index) => (
        <List.Item>
          <Card title={index + 1}>{item.move.name}</Card>
        </List.Item>
      )} />
  )
}

export default MovesItem;